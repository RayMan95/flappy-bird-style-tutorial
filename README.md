Unity game made following the Flappy Bird Style tutorial (https://learn.unity.com/tutorial/live-session-making-a-flappy-bird-style-game?courseId=5c5c1e08edbc2a5465c7ec01#5c7f8528edbc2a002053b69e)

Most image assets are not my property and almost everything else was made following the tutorial. Some things were my flavourful additions.