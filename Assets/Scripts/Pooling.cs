﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pooling : MonoBehaviour
{
    private GameObject[] columns;
    public GameObject columnPrefab;
    public int columnPoolSize = 5;
    private int currentColumn = 0;

    private Vector2 objectPoolPosition = new Vector2(-35f, -35f);

    private float timeSinceLastSpawn;
    public float spawnRate = 3f;

    public float columnMin = 0f;
    public float columnMax = 4f;
    private float spawnXPosition = 10f;

    // Start is called before the first frame update
    void Start()
    {
        timeSinceLastSpawn = 0f;
        columns = new GameObject[columnPoolSize];
        for (int i = 0; i < columnPoolSize; i++)
        {
            columns[i] = (GameObject)Instantiate(columnPrefab, objectPoolPosition, Quaternion.identity);
        }
    }

    // Update is called once per frame
    void Update()
    {
        timeSinceLastSpawn += Time.deltaTime;

        if(!GameControl.instance.GameOver && timeSinceLastSpawn > spawnRate)
        {
            timeSinceLastSpawn = 0f;
            float spawnYPosition = Random.Range(columnMin, columnMax);
            columns[currentColumn].transform.position = new Vector2(spawnXPosition, spawnYPosition);
            currentColumn++;

            if (currentColumn >= columnPoolSize)
            {
                currentColumn = 0;
            }
        }
    }
}
