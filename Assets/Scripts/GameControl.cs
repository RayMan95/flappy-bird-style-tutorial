﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameControl : MonoBehaviour
{
    public static GameControl instance;

    public Text scoreText;
    public GameObject gameOverText;

    public int score = 0;
    private bool gameOver = false;
    public float scrollSpeed = -1.5f;

    public bool GameOver 
    { 
        get { return gameOver; }
        set { gameOver = value; } 
    }

    // Start is called before the first frame update
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (GameOver == true && Input.GetMouseButtonDown(0))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
        }
    }

    public void BirdScored()
    {
        if (GameOver) return;

        score++;

        scoreText.text = "Score: " + score.ToString();
    }

    public void BirdDied ()
    {
        gameOverText.SetActive(true);
        GameOver = true;
    }
}
