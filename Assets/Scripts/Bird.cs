﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour
{
    public float upForce = 200f;
    private bool isDead = false; 

    private Rigidbody2D rb2d;
    private Animator animator;

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isDead)
        {
            if (Input.GetMouseButtonDown(0))
            {
                animator.SetTrigger("Flap");

                rb2d.velocity = Vector2.zero;
                rb2d.AddForce(new Vector2(0, upForce));
            }
        }
    }

    void OnCollisionEnter2D (Collision2D other)
    {
        if (other.gameObject.GetComponent<Column>() == null) 
        {
            rb2d.angularVelocity = 0f;
            return;
        }
        

        isDead = true;
        Debug.Log("Dead at : " + transform.position);
    
        animator.SetTrigger("Die");

        GameControl.instance.BirdDied();
    }
}
